## Introduce
- This shell project is for spliting router in neutron network node.
- There are two l3 agents in default neutron environment.
- The first l3 agent provide internal net service.
- The second l3 agent provide external net service.

## How to use
1. Configure the file vrouter.cfg firstly.
   - net_in_host for the host running the first l3 agent
   - net_ex_host for the host running the second l3 agent
   - net_in_ip for the ip of host running the first l3 agent
   - net_ex_ip for the ip of host running the second l3 agent

2. Create certified ssh channels between current host to the hosts running l3 agent. This operation is for avoiding to input your password when running there scripts.

3. export your admin keystore settings

4. run your wanted vrouter command under bin folder  
   for example: ./vrouter-create myrouter

## Copyright
> All Rights Reserved.

